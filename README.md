# Générateur de plateau de service

## Description

Ce projet permet de matérialiser cet objet que j'utilise tous les jours. Je fournis à la fois un plan de découpe laser prêt à l'emploi et sa source. La source permet de personnaliser toutes les dimensions d'un plateau, puis de générer de nouveaux plans et d'obtenir un objet à votre goût.

Vous trouverez toutes les infos complémentaires dans [l'article associé](http://influence-pc.fr/18-08-2017-un-generateur-de-plateau-a-la-decoupe-laser).

![Plateau de service](http://influence-pc.fr/envois/IMG_20170817_102859-950x536.jpg)

## Installation

- Télécharger [OpenSCAD](http://www.openscad.org/downloads.html)

## Utilisation

En ouvrant **tray.scad** dans le logiciel OpenSCAD, vous avez la possibilité de configurer quelques variables :

    lateral_planks_thickness = 10;
    bottom_plank_thickness = 4.2; // Add 0.5mm to the size as a mounting marge.
    bottom_PMMA_thickness = 4; // Optional transparent protection at bottom of the tray.

L'épaisseur des matériaux en millimètres selon les plans, pour les montants latéraux, le fond et une éventuelle protection à placer au fond à l'intérieur du plateau. Le PMMA est le nom générique du Plexiglass, j'ai imaginé la possibilité de placer des imprimés interchangeables sous une plaque transparente.

    tray_length = 480; // Ratio 16:10 by default.
    tray_width = 300;
    tray_height = 50;

Les dimensions de votre plateau (longueur, largeur, hauteur) en millimètres. Le ratio 16:10 offre des proportions pratiques à l'usage, mais ce n'est une geekerie :)

    handle_length = 80;
    handle_height = 20;

Les dimensions des poignées, en longeur et en hauteur.

    add_bottom_PMMA_plate = true; // Set to "false" to disable.

Ajoute la protection optionnelle à la prévisualisation et aux plans. Je n'ai pas eu l'occasion de l'essayer, n'hésitez pas à me faire remonter si ses dimensions ne frottent pas sur les bords du plateau.

    display = "3D"; // Set to "2D" to see a plane view.

Le rendu 3D théorique est chargé par défaut. En pratique, ce rendu va dépendre de l'épaisseur du laser (de la matière est vaporisée) et du montage de votre plateau. On trouve rarement du bois parfaitement plat sur une longueur de 50cm. Passer la variable à "2D" vous donne un aperçu du plan final.

Lors de l'assemblage, un certain jeu ou au contraire d'une difficulté à emboiter les éléments va survenir. Cette imprécision est dûe principalement à la précision de la mesure de vos matériaux, ainsi que de la dureté du bois utilisé.

Vous pouvez ensuite générer vos plans en ouvrant un terminal, en vous déplaçant dans le dossier du projet en exécutant :

    bash convert-2d.sh tray.scad

Vous pouvez aussi utiliser le menu *Fichier* / *Exporter* pour obtenir un fichier vectoriel SVG standard ou un dxf.

## Remerciements

Je tiens à remercier Brendan M. Sleight pour sa bibliothèque [lasercut](https://github.com/bmsleight/lasercut) permettant de créer très facilement des plans 2D pour découpeuse laser à partir de modélisations 3D dans OpenSCAD.

## Licence

Ceci est un logiciel libre placé sous [licence CeCILL-B](http://www.cecill.info/licences/Licence_CeCILL-B_V1-fr.html), inspirée par la licence BSD 2-Clause et adaptée au droit français. Elle vous permet d'utiliser, copier, modifier et partager ce projet, usage commercial inclus, à la principale contrainte de respecter la paternité du projet. Vous devez associer le lien suivant [http://influence-pc.fr/18-08-2017-un-generateur-de-plateau-a-la-decoupe-laser](http://influence-pc.fr/18-08-2017-un-generateur-de-plateau-a-la-decoupe-laser) à toute réalisation ou publication de l'oeuvre, initiale ou dérivée, ainsi que conserver les entêtes de licence placées dans le code source. J'espère que cela vous va :)